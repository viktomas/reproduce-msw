const fetch = require("node-fetch");

const run = async () => {
  const response = await fetch("https://test.gitlab.com/version");
  return response.text();
};

module.exports = { run };
