const { run } = require(".");
const { rest } = require("msw");
const { setupServer } = require("msw/node");

describe("example", () => {
  let server;

  it("run", async () => {
    const endpoint = rest.get(
      "https://test.gitlab.com/version",
      (req, res, ctx) => res(ctx.status(200), ctx.text("abc"))
    );
    server = setupServer(endpoint);
    server.listen();

    const result = await run();
    expect(result).toBe("abc");
  });
});
